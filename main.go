package main

import (
	"coba/auth"
	"coba/handler"
	"coba/helper"
	"coba/kategori"
	"coba/user"
	"log"
	"net/http"
	"strings"

	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

const (
	MysqlDsn = `root:@/colearn?parseTime=True&charset=utf8`
)

func main() {
	var err error
	db, err := gorm.Open(mysql.Open(MysqlDsn), &gorm.Config{})

	if err != nil {
		log.Fatal(err.Error())
	}

	//api.GET("/users.fetch", authMiddleware(authService, userService), userHandler.FetchUser)
	//handler, mapping input dari user -> struct input
	//service: melakukan mapping dari struct input ke struct

	userRepository := user.NewRepository(db)
	userService := user.NewService(userRepository)
	authService := auth.NewService()

	kategoriRepository := kategori.NewRepository(db)
	kategoriService := kategori.NewService(kategoriRepository)

	userHandler := handler.NewUserHandler(userService, authService)
	kategoriHandler := handler.NewKategoriHandler(kategoriService, authService)

	router := gin.Default()
	api := router.Group("/api/v1")

	api.POST("/users", userHandler.RegisterUser)
	api.POST("/sessions", userHandler.Login)
	api.POST("/email_checker", userHandler.CheckEmailAvailibility)
	api.POST("/avatars", authMiddleware(authService, userService), userHandler.UploadAvatar)
	api.GET("users/fetch", authMiddleware(authService, userService), userHandler.FetchUser)

	api.POST("/kategori", kategoriHandler.InsertKategori)

	router.Run(":8448")

}

func authMiddleware(authService auth.Service, userService user.Service) gin.HandlerFunc {
	return func(c *gin.Context) {
		authHeader := c.GetHeader("Authorization")

		if !strings.Contains(authHeader, "Bearer") {
			response := helper.APIResponse("Unautharized", http.StatusUnauthorized, "error", nil)
			c.AbortWithStatusJSON(http.StatusUnauthorized, response)
			return
		}

		tokenString := ""
		arrayToken := strings.Split(authHeader, " ")
		if len(arrayToken) == 2 {
			tokenString = arrayToken[1]
		}

		token, err := authService.ValidateToken(tokenString)
		if err != nil {
			response := helper.APIResponse("Unauthorized", http.StatusUnauthorized, "error", nil)
			c.AbortWithStatusJSON(http.StatusUnauthorized, response)
			return
		}

		claim, ok := token.Claims.(jwt.MapClaims)

		if !ok || !token.Valid {
			response := helper.APIResponse("Unauthorized", http.StatusUnauthorized, "error", nil)
			c.AbortWithStatusJSON(http.StatusUnauthorized, response)
			return
		}

		userID := int(claim["user_id"].(float64))

		user, err := userService.GetUserByID(userID)
		if err != nil {
			response := helper.APIResponse("Unauthorized", http.StatusUnauthorized, "error", nil)
			c.AbortWithStatusJSON(http.StatusUnauthorized, response)
			return
		}

		c.Set("currentUser", user)
	}
}

//ambil nilai header authorization (jika user mngrm req yg butuh author, di header akan mengirim authorization ) value nya Bearer tokentokentokne
//dari header author, amnbil nilai tokennya saja
//kita validasi token mnggunan serviece validate token
// ambil user_id
// ambil user dari db berdasarkan user_id lewat service
// set context isi user
