module coba

go 1.15

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gin-gonic/gin v1.6.3
	github.com/go-playground/validator/v10 v10.4.1
	github.com/jinzhu/gorm v1.9.16
	golang.org/x/crypto v0.0.0-20200622213623-75b288015ac9
	gorm.io/driver/mysql v1.0.3
	gorm.io/gorm v1.20.6
)
