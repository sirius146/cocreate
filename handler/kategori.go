package handler

import (
	"coba/auth"
	"coba/helper"
	"coba/kategori"
	"net/http"

	"github.com/gin-gonic/gin"
)

type kategoriHandler struct {
	kategoriService kategori.Service
	authService     auth.Service
}

func NewKategoriHandler(kategoriService kategori.Service, authService auth.Service) *kategoriHandler {
	return &kategoriHandler{kategoriService, authService}
}

func (h *kategoriHandler) InsertKategori(c *gin.Context) {
	//tangkap input user
	//map input dari user ke struct registeruserinput
	//struct diatas kita passing sbg parameter service

	var input kategori.InsertKatInput

	err := c.ShouldBindJSON(&input)

	if err != nil {
		errors := helper.FormatValidationError(err)
		//gin.h map keynya string valuenya interface(bisa apa aja)
		errorMessage := gin.H{"errors": errors}

		response := helper.APIResponse("input kategori failed", http.StatusUnprocessableEntity, "error", errorMessage)
		c.JSON(http.StatusUnprocessableEntity, response)
		return
	}

	newKategori, err := h.kategoriService.InsertKat(input)
	if err != nil {
		response := helper.APIResponse("insert kategori failed", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return
	}

	response := helper.APIResponse("kategori has been registered", http.StatusOK, "success", newKategori)
	c.JSON(http.StatusOK, response)
}

/* func (h *userHandler) FetchKategori(c *gin.Context) {

	var ka []kategori.Kategori

	res := kategori.FetchKat(ka)

	response := helper.APIResponse("successfully fetch user data", http.StatusOK, "success", res)

	c.JSON(http.StatusOK, response)

}
*/
