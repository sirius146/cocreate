package user

import "time"

type User struct {
	ID        int       `gorm:"primary_key";auto_increment;not_null json:"-"`
	Username  string    `json:"Username"`
	Password  string    `json:"password,omitempty"`
	Nama      string    `json:"name,omitempty"`
	Email     string    `json:"email,omitempty"`
	Phone     string    `json:"phone,omitempty"`
	Ttl       string    `json:"ttl,omitempty"`
	Photo     string    `json:"photo,omitempty"`
	CreatedAt time.Time `json:"created_at, omitempty"`
	UpdatedAt time.Time `json:"updated_at, omitempty"`
}
