package user

type UserFormatter struct {
	ID       int    `json:"id"`
	Username string `json:"username"`
	Nama     string `json:"nama"`
	Email    string `json:"email"`
	Token    string `json:"token"`
	ImageURL string `json:"image_url"`
}

func FormatUser(user User, token string) UserFormatter {
	formatter := UserFormatter{
		ID:       user.ID,
		Username: user.Username,
		Nama:     user.Nama,
		Email:    user.Email,
		Token:    token,
		ImageURL: user.Photo,
	}

	return formatter
}
