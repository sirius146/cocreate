package kategori

type Kategori struct {
	ID      int    `gorm:"primary_key";auto_increment;not_null json:"-"`
	NamaKat string `json:"namakat,omitempty"`
}
