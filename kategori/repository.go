package kategori

import "gorm.io/gorm"

type Repository interface {
	Save_kat(kategori Kategori) (Kategori, error)
}

type repository struct {
	db *gorm.DB
}

func NewRepository(db *gorm.DB) *repository {
	return &repository{db}
}

func (r *repository) Save_kat(kategori Kategori) (Kategori, error) {
	err := r.db.Create(&kategori).Error
	if err != nil {
		return kategori, err
	}

	return kategori, nil
}

/* func (r *repository) FetchKat(katlist []Kategori) ([]Kategori, error) {
	err := r.db.Find(&katlist).Error
	if err != nil {
		return katlist, err
	}

	return katlist, nil
} */
