package kategori

type Service interface {
	InsertKat(input InsertKatInput) (Kategori, error)
	ShowKat(input InsertKatInput) (Kategori, error)
}

type service struct {
	repository Repository
}

func NewService(repository Repository) *service {
	return &service{repository}
}

func (s *service) InsertKat(input InsertKatInput) (Kategori, error) {
	kategori := Kategori{}
	kategori.NamaKat = input.NamaKat

	newKategori, err := s.repository.Save_kat(kategori)
	if err != nil {
		return kategori, err
	}

	return newKategori, nil
}
